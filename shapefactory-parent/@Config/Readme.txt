Pasos para la instalacion del sistema:
--------------------------------------
1) Instalar MySQL 5.7
a- Crear usuario "geopagos" con contraseña "g30p4g0s"
b- Ejecutar script de creacion de base de datos "init_database.sql"
c- Darle permisos al usuario "geopagos" sobre la base de datos "shape_factory"

2) Instalar Tomcat 9


Endpoints:
----------
POST	http://localhost:8080/shapefactory-rest/rest/shapeService/createShape
POST	http://localhost:8080/shapefactory-rest/rest/shapeService/getShapeById
PUT		http://localhost:8080/shapefactory-rest/rest/shapeService/updateShape
DELETE	http://localhost:8080/shapefactory-rest/rest/shapeService/deleteShape