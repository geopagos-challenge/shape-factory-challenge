package com.geopagos.challenge.shapefactory.rest.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.geopagos.challenge.shapefactory.core.dto.ShapeDto;
import com.geopagos.challenge.shapefactory.core.entity.AbstractShape;
import com.geopagos.challenge.shapefactory.core.entity.Shape;
import com.geopagos.challenge.shapefactory.core.logic.ShapeLogic;

@Component
@Path("/shapeService")
public class ShapeCRUDService {
	
	@Autowired(required=true)
	@Qualifier("shapeLogic")
	private ShapeLogic shapeLogic;
	
    @GET
    @Path("/ping")
    @Produces(MediaType.TEXT_PLAIN)
    public String ping() {
    	return "PING!!";
    }
    
    @POST
    @Path("/createShape")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createShape(String shapeName) {
    	ShapeDto shapeDto = this.shapeLogic.createShape(shapeName);
    	return Response.status(Response.Status.ACCEPTED)
				.entity(shapeDto)
				.build();
    } 
    
    @POST
    @Path("/getShapeById")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getShapeById(long shapeId) {
    	ShapeDto shapeDto = this.shapeLogic.getShapeDto(shapeId);
    	return Response.status(Response.Status.ACCEPTED)
				.entity(shapeDto)
				.build();
    } 
    
    @PUT
    @Path("/updateShape")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateShape(ShapeDto shape) {
    	this.shapeLogic.updateShape(shape);
    	return Response.status(Response.Status.ACCEPTED)
				.build();
    } 
    
    @DELETE
    @Path("/deleteShape")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteShape(long shapeId) {
    	this.shapeLogic.deleteShape(shapeId);
    	return Response.status(Response.Status.ACCEPTED)
				.build();
    }
}
