package com.geopagos.challenge.shapefactory.core.entity;

import javax.persistence.Entity;

@Entity
public class Triangle extends AbstractShape{
	public Triangle() {
		this.type = ShapeType.TRIANGLE;
		this.base = 2.0;
		this.height = 2.0;
	}

	@Override
	public Double getArea() {
		return base * height / 2;
	}

	@Override
	public Double getDiameter() {
		return null;
	}
}
