package com.geopagos.challenge.shapefactory.core.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

//@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Repository
public abstract class GenericDao<T, I extends Serializable> {
	@PersistenceContext
	protected EntityManager entityManager;

	private Class<T> clazz;

	public GenericDao() {

	}

	public void setClazz(Class<T> clazzToSet) {
		this.clazz = clazzToSet;
	}

	public T findOne(I id) {
		return entityManager.find(clazz, id);
	}
	
	public List<T> findAll() {
		return entityManager.createQuery("from " + clazz.getName()).getResultList();
	}

	public T save(T entity) {
		entityManager.persist(entity);
		return entity;
	}

	public T update(T entity) {
		return entityManager.merge(entity);
	}

	public void delete(T entity) {
		entityManager.remove(entity);
	}

	public void deleteById(I entityId) {
		T entity = findOne(entityId);
		delete(entity);
	}

	public void executeNativeUpdateQuery(String nativeQuery) {
		Query query = entityManager.createNativeQuery(nativeQuery);
		query.executeUpdate();
	}

	
}
