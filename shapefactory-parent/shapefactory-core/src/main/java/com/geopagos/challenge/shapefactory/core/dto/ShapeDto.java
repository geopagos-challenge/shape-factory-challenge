package com.geopagos.challenge.shapefactory.core.dto;

public class ShapeDto {
	protected Long id;
	protected String type;
	protected Double base;
	protected Double height;
	protected Double diameter;
	protected Double area;
	
	public ShapeDto() {}
	
	public ShapeDto(Long id, String type, Double base, Double height, Double diameter, Double area) {
		super();
		this.id = id;
		this.type = type;
		this.base = base;
		this.height = height;
		this.diameter = diameter;
		this.area = area;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Double getBase() {
		return base;
	}
	public void setBase(Double base) {
		this.base = base;
	}
	public Double getHeight() {
		return height;
	}
	public void setHeight(Double height) {
		this.height = height;
	}
	public Double getDiameter() {
		return diameter;
	}
	public void setDiameter(Double diameter) {
		this.diameter = diameter;
	}
}
