package com.geopagos.challenge.shapefactory.core.logic.factory;

import com.geopagos.challenge.shapefactory.core.entity.Shape;

public abstract class AbstractShapeFactory {
	public abstract Shape createShape(String shapeName);
	protected abstract Shape createSquare();
	protected abstract Shape createTriangle();
	protected abstract Shape createCircle();
}
