package com.geopagos.challenge.shapefactory.core.dto.assembler;

import java.util.ArrayList;
import java.util.List;

import com.geopagos.challenge.shapefactory.core.dto.ShapeDto;
import com.geopagos.challenge.shapefactory.core.entity.AbstractShape;
import com.geopagos.challenge.shapefactory.core.entity.Circle;
import com.geopagos.challenge.shapefactory.core.entity.Shape;
import com.geopagos.challenge.shapefactory.core.entity.Shape.ShapeType;
import com.geopagos.challenge.shapefactory.core.entity.Square;
import com.geopagos.challenge.shapefactory.core.entity.Triangle;

public class ShapeAssembler {

	public static ShapeDto toDto(AbstractShape shape) {
		return new ShapeDto(shape.getId(), shape.getType(), shape.getBase(), shape.getHeight(), shape.getDiameter(), shape.getArea());
	}

	public static List<ShapeDto> toDtoList(List<AbstractShape> shapeList) {
		List<ShapeDto> dtoList = new ArrayList<>();
		for (int i = 0; i < shapeList.size(); i++) {
			dtoList.add(toDto(shapeList.get(i)));
		}
		return dtoList;
	}
}
