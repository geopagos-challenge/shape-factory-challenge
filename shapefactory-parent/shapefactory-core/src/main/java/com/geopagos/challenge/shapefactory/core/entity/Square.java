package com.geopagos.challenge.shapefactory.core.entity;

import javax.persistence.Entity;

@Entity
public class Square extends AbstractShape{
	public Square() {
		this.type = ShapeType.SQUARE;
		this.base = 2.0;
		this.height = 2.0;
	}

	@Override
	public Double getArea() {
		return base * height;
	}

	@Override
	public Double getDiameter() {
		return null;
	}
}
