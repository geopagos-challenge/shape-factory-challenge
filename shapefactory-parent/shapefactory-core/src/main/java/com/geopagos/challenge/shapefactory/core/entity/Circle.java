package com.geopagos.challenge.shapefactory.core.entity;

import javax.persistence.Entity;

@Entity
public class Circle extends AbstractShape{
	public Circle() {
		this.type = ShapeType.CIRCLE;
		this.diameter = 2.0;
	}

	@Override
	public Double getArea() {
		return Math.PI * Math.pow(diameter/2,2);
	}

	@Override
	public Double getBase() {
		return null;
	}

	@Override
	public Double getHeight() {
		return null;
	}
}
