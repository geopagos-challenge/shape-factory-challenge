package com.geopagos.challenge.shapefactory.core.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.geopagos.challenge.shapefactory.core.entity.AbstractShape;

@Repository("shapeDao")
@Transactional(propagation=Propagation.REQUIRED)
public class ShapeDao extends GenericDao<AbstractShape, Long>{

	
}
