package com.geopagos.challenge.shapefactory.core.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name="shape")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class AbstractShape implements Shape{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	protected Long id;
	protected ShapeType type;
	protected Double base;
	protected Double height;
	protected Double diameter;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getType() {
		return type.toString();
	}
	public void setType(ShapeType type) {
		this.type = type;
	}
	public Double getBase() {
		return base;
	}
	public void setBase(Double base) {
		this.base = base;
	}
	public Double getHeight() {
		return height;
	}
	public void setHeight(Double height) {
		this.height = height;
	}
	public Double getDiameter() {
		return diameter;
	}
	public void setDiameter(Double diameter) {
		this.diameter = diameter;
	}
}
