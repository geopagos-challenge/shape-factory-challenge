package com.geopagos.challenge.shapefactory.core.entity;

public interface Shape {
	public enum ShapeType{
		SQUARE, TRIANGLE, CIRCLE;
	}
	
	public Double getArea();
	public Double getBase();
	public Double getHeight();
	public Double getDiameter();
	public String getType();
}
