package com.geopagos.challenge.shapefactory.core.dao;

import java.io.Serializable;
import java.util.List;

public interface IGenericDao<T, I extends Serializable> {
	T findOne(final I id);

	List<T> findAll();

	T save(final T entity);

	T update(final T entity);

	void delete(final T entity);

	void deleteById(final I entityId);

	void executeNativeUpdateQuery(final String nativeQuery);
		
	void setClazz(Class<T> clazz);
}
