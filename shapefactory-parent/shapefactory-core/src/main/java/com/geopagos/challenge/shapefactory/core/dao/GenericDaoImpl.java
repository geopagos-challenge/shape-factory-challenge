package com.geopagos.challenge.shapefactory.core.dao;

import java.io.Serializable;

import org.springframework.stereotype.Repository;

@Repository
public class GenericDaoImpl<T, I extends Serializable> extends GenericDao<T, I> implements IGenericDao<T, I> {

}
