package com.geopagos.challenge.shapefactory.core.logic.factory;

import org.springframework.stereotype.Component;

import com.geopagos.challenge.shapefactory.core.entity.Circle;
import com.geopagos.challenge.shapefactory.core.entity.Shape;
import com.geopagos.challenge.shapefactory.core.entity.Shape.ShapeType;
import com.geopagos.challenge.shapefactory.core.entity.Square;
import com.geopagos.challenge.shapefactory.core.entity.Triangle;

@Component("concreteShapeFactory")
public class ConcreteShapeFactory extends AbstractShapeFactory{
	
	@Override
	public Shape createShape(String shapeName) {
		Shape shape = null;
		switch(ShapeType.valueOf(shapeName)) {
		case SQUARE:
			shape = this.createSquare();
			break;
		case TRIANGLE:
			shape = this.createTriangle();
			break;
		case CIRCLE:
			shape = this.createCircle();
			break;
		default:
			throw new IllegalArgumentException();
		}
		
		return shape;
	}

	@Override
	protected Shape createSquare() {
		return new Square();
	}

	@Override
	protected Shape createTriangle() {
		return new Triangle();
	}

	@Override
	protected Shape createCircle() {
		return new Circle();
	}

}
