package com.geopagos.challenge.shapefactory.core.logic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.geopagos.challenge.shapefactory.core.dao.IGenericDao;
import com.geopagos.challenge.shapefactory.core.dto.ShapeDto;
import com.geopagos.challenge.shapefactory.core.dto.assembler.ShapeAssembler;
import com.geopagos.challenge.shapefactory.core.entity.AbstractShape;
import com.geopagos.challenge.shapefactory.core.entity.Shape;
import com.geopagos.challenge.shapefactory.core.entity.Shape.ShapeType;
import com.geopagos.challenge.shapefactory.core.logic.factory.AbstractShapeFactory;

@Service("shapeLogic")
@Transactional(propagation = Propagation.REQUIRED)
public class ShapeLogic {

	private IGenericDao<AbstractShape, Long> shapeDao;
	private AbstractShapeFactory shapeFactory;

	public ShapeDto createShape(String shapeName) {
		Shape shape = this.shapeFactory.createShape(shapeName);
		return ShapeAssembler.toDto(this.shapeDao.save((AbstractShape)shape));
	}
	
	public ShapeDto getShapeDto(long id) {
		return ShapeAssembler.toDto(this.shapeDao.findOne(id));
	}
	
	private Shape getShape(long id) {
		return this.shapeDao.findOne(id);
	}
	
	public Shape updateShape(ShapeDto shapeDto) {
		AbstractShape shape = (AbstractShape)getShape(shapeDto.getId());
		if(!shape.getType().equals(shapeDto.getType())) {
			String type = shapeDto.getType().substring(0, 1) + shapeDto.getType().substring(1).toLowerCase();
			this.shapeDao.executeNativeUpdateQuery("UPDATE shape SET DTYPE = \"" + type + "\" WHERE id = " + shapeDto.getId());
		}

		shape.setBase(shapeDto.getBase());
		shape.setHeight(shapeDto.getHeight());
		shape.setDiameter(shapeDto.getDiameter());
		shape.setType(ShapeType.valueOf(shapeDto.getType()));
		return this.shapeDao.update(shape);
	}

	public void deleteShape(long id) {
		this.shapeDao.deleteById(id);;
	}
	
	@Autowired
	@Qualifier("concreteShapeFactory")
	public void setShapeFactory(AbstractShapeFactory shapeFactory) {
		this.shapeFactory = shapeFactory;
	}
	
	@Autowired
	public void setShapeDao(IGenericDao<AbstractShape, Long> shapeDao) {
		this.shapeDao = shapeDao;
		this.shapeDao.setClazz(AbstractShape.class);
	}
}
